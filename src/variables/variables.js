import Plugin from "@ckeditor/ckeditor5-core/src/plugin";
import VariablesEditing from "./variablesediting";
import VariablesUI from "./variablesui";

export default class Variables extends Plugin {
    static get requires() {
        return [VariablesEditing, VariablesUI];
    }

    static get pluginName() {
        return "variable";
    };
};
