import {toWidget, viewToModelPositionOutsideModelElement} from '@ckeditor/ckeditor5-widget/src/utils';
import Plugin from "@ckeditor/ckeditor5-core/src/plugin";
import {Widget} from "@ckeditor/ckeditor5-widget";

import VariablesCommand from "./variablescommand";


export default class VariablesEditing extends Plugin {
    static get requires() {
        return [Widget];
    }

    static get pluginName () {
        return "VariableEditing";
    };

    init() {
        this._defineSchema();
        this._defineConverters();

        this.editor.commands.add('variable', new VariablesCommand(this.editor));
        this.editor.editing.mapper.on(
            'viewToModelPosition',
            viewToModelPositionOutsideModelElement( this.editor.model, viewElement => viewElement.hasClass( 'variable' ) )
        );

        // Настройки по умолчанию
        this.editor.config.define( 'variablesConfig', {
            // ADDED
            data: []
        } );
    }

    _defineSchema () {
        const schema = this.editor.model.schema;

        this.editor.model.schema.register("variable", {
            // Allow wherever text is allowed:
            allowWhere: "$text",

            // The placeholder will act as an inline node:
            isInline: true,

            // The inline widget is self-contained so it cannot be split by the caret and can be selected:
            isObject: true,

            // The inline widget can have the same attributes as text (for example linkHref, bold).
            allowAttributesOf: '$text',

            // The placeholder can have many types, like date, name, surname, etc:
            allowAttributes: [ 'name' ]
        });
    }

    _defineConverters() {
        const conversion = this.editor.conversion;

        conversion.for( 'upcast' ).elementToElement( {
            view: {
                name: 'span',
                classes: [ 'variable' ]
            },
            model: ( viewElement, { writer: modelWriter } ) => {
                // Удаляем скобки от переменной,
                // чтобы из {{first_name}} получить first_name
                const name = viewElement.getChild( 0 ).data.slice( 2, -2 );

                return modelWriter.createElement( 'variable', { name } );
            }
        } );

        conversion.for( 'editingDowncast' ).elementToElement( {
            model: 'variable',
            view: ( modelItem, { writer: viewWriter } ) => {
                const widgetElement = createPlaceholderView( modelItem, viewWriter );

                // Enable widget handling on a placeholder element inside the editing view.
                return toWidget( widgetElement, viewWriter );
            }
        } );

        conversion.for( 'dataDowncast' ).elementToElement( {
            model: 'variable',
            view: ( modelItem, { writer: viewWriter } ) => createPlaceholderView( modelItem, viewWriter )
        } );
    }
};

// Helper method for both downcast converters.
function createPlaceholderView( modelItem, viewWriter ) {
    const name = modelItem.getAttribute( 'name' );

    const variableView = viewWriter.createContainerElement( 'span', {
        class: 'variable'
    } );

    // Insert the variable name (as a text).
    const innerText = viewWriter.createText( '{{' + name + '}}' );
    viewWriter.insert( viewWriter.createPositionAt( variableView, 0 ), innerText );

    return variableView;
}
